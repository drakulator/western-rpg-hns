﻿[System.Serializable]
public struct ItemQuantity
{
    public Item item;
    public int quantity;
    public bool IsEmpty => (item == null | quantity <= 0);

    public ItemQuantity(Item item, int quantity = 1)
    {
        this.item = item;
        this.quantity = quantity;
    }

    public void EmptySlot()
    {
        item = null;
        quantity = 0;
    }
}
