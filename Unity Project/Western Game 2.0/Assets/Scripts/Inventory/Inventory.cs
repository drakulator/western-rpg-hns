﻿using NaughtyAttributes;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField]
    private ItemQuantity[] items = new ItemQuantity[GameConsts.MAX_INVENTORY_SIZE];
    [SerializeField]
    private int money;

    private int indexToSplit;

    public ItemQuantity[] Items
    {
        get
        {
            return items;
        }
    }

    public int Money
    {
        get
        {
            return money;
        }
    }

    [SerializeField]
    private InventoryType inventoryType;
    public InventoryType InventoryType { get { return inventoryType; } }

    private void OnEnable()
    {
        EventManager.AddListener<InventoryType>("InventoryOpened", OnInventoryOpened);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<InventoryType>("InventoryOpened", OnInventoryOpened);
    }

    private void OnInventoryOpened(object sender, InventoryType inventoryType)
    {
        if (this.inventoryType == InventoryType.Player || this.inventoryType == inventoryType)
        {
            LoadInventory();
        }
    }

    private void ReplaceItems(ItemQuantity itemQuantity, int slotIndex)
    {
        items[slotIndex] = itemQuantity;
    }

    private void AccumulateItems(ItemQuantity itemQuantity, int slotIndex)
    {
        items[slotIndex].quantity += itemQuantity.quantity;
    }

    private void AddItemsToEmptySlot(Item item, int quantity)
    {
        int index = FindEmptySlot();
        items[index].item = item;
        items[index].quantity = quantity;
    }

    private void RemoveItems(int quantity, int slotIndex)
    {
        if(items[slotIndex].quantity > quantity)
        {
            items[slotIndex].quantity -= quantity;
        }
        else
        {
            items[slotIndex].EmptySlot();
        }
    }

    private void EmptySlot(int index)
    {
        items[index].EmptySlot();
    }

    public bool Contains(Item item)
    {
        foreach (ItemQuantity itemQuantity in items)
        {
            if (itemQuantity.item == item)
                return true;
        }
        return false;
    }

    public ItemQuantity GetItem(int index)
    {
        return items[index];
    }

    public int FindEmptySlot()
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i].IsEmpty)
                return i;
        }
        return -1;
    }

    [Button]
    private void LoadInventory()
    {
        EventManager.Invoke("InventoryLoaded", this);
    }

    public static void ExchangeItems(Inventory origin, int originIndex, Inventory target, int targetIndex)
    {
        if (origin.items[originIndex].IsEmpty)
            return;
        ItemQuantity originItemQuantity = origin.items[originIndex];
        if (target.items[targetIndex].IsEmpty)
        {
            origin.EmptySlot(originIndex);
            target.ReplaceItems(originItemQuantity, targetIndex);
        }
        else
        {
            ItemQuantity targetItemQuantity = target.items[targetIndex];
            if (Item.CanBeStacked(originItemQuantity.item, targetItemQuantity.item))
            {
                origin.EmptySlot(originIndex);
                target.AccumulateItems(originItemQuantity, targetIndex);
            }
            else
            {
                target.ReplaceItems(originItemQuantity, targetIndex);
                origin.ReplaceItems(targetItemQuantity, originIndex);
            }
        }
    }

    public void ChangeMoney(int amount)
    {
        money = Mathf.Clamp(money + amount, 0, GameConsts.MAX_MONEY);
        EventManager.Invoke("MoneyChanged", this, money);
    }

    public void ExecuteAction(int index, InventoryActionType action)
    {
        switch (action)
        {
            case InventoryActionType.Use:
                break;
            case InventoryActionType.Split:
                indexToSplit = index;
                EventManager.Invoke("SplitPanelOpened", this, items[index].quantity);
                EventManager.AddListener<int>("SplitPanelValuesSelected", OnSplitPanelValuesSelected);
                break;
            case InventoryActionType.Remove:
                EmptySlot(index);
                EventManager.Invoke("InventoryChanged", this);
                break;
            case InventoryActionType.Cancel:
                break;
        }
    }

    private void OnSplitPanelValuesSelected(object sender, int value)
    {
        EventManager.RemoveListener<int>("SplitPanelValuesSelected", OnSplitPanelValuesSelected);
        RemoveItems(value, indexToSplit);
        AddItemsToEmptySlot(items[indexToSplit].item, value);
        EventManager.Invoke("InventoryChanged", this);
    }
}