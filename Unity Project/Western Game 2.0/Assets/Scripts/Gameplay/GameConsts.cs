﻿public static class GameConsts
{
    public const float MAX_RESISTANCE = 0.8f;
    public const float MAX_VULNERABILITY = -1f;
    public const int MAX_INVENTORY_SIZE = 20;
    public const float INTERACTION_RANGE = 5f;
    public const float IN_COMBAT_TIME = 5f;
    public const int MAX_MONEY = 99999;
}
