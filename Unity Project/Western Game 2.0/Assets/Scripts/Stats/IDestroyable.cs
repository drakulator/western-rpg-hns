﻿public interface IDestroyable
{
    void GiveDamage(DamageType type, int damage, bool criticalHit = false, Effect[] additionalEffects = null);
}
