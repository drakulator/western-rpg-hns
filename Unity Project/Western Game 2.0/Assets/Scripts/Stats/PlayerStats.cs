﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerBaseStats))]
public class PlayerStats : Stats
{
    private PlayerBaseStats baseStats;

    private Inventory inventory;

    public bool WeaponEquipped { get; } = true;

    public int Damage { get { return damage; } }
    public float AttackSpeed { get { return attackSpeed; } }
    public float Range { get { return range; } }
    public float CritChance { get { return critChance; } }
    public Effect[] AttackEffects { get { return attackEffects; } }
    public DamageType DamageType { get { return damageType; } }

    // Use this for initialization
    void Start () {
        baseStats = GetComponent<PlayerBaseStats>();

        InitiateStats();
        CheckConstraints();
        UpdateAgent();

        InvokeRepeating("RegenerateHealth", 1f, 1f);
    }

    private void OnEnable()
    {
        EventManager.AddListener("PlayerBaseStats_Changed", OnBaseStatsChanged);
        EventManager.AddListener("PlayerBaseStats_ProficienciesChanged", OnBaseStatsChanged);
    }

    private void OnDisable ()
    {
        EventManager.RemoveListener("PlayerBaseStats_Changed", OnBaseStatsChanged);
        EventManager.RemoveListener("PlayerBaseStats_ProficienciesChanged", OnBaseStatsChanged);
    }

    private void OnBaseStatsChanged(object sender)
    {
        RecalculateStats();
    }

    private void InitiateStats()
    {
        maxHealth = baseStats.maxHealth;
        health = baseStats.maxHealth;
        physicalResistance = 0;
        fireResistance = 0;
        lightningResistance = 0;
        poisonResistance = 0;
        damageEffect = baseStats.damageEffect;
        deathEffect = baseStats.deathEffect;
        healthRegen = baseStats.healthRegen;
        moveSpeed = baseStats.moveSpeed;
        angularSpeed = baseStats.angularSpeed;
        damageType = DamageType.Physical;
        //for now
        //------------------
        damage = 10;
        attackSpeed = 1f;
        range = 3f;
        critChance = 0.01f;
        //------------------
        effects = new List<Effect>();
    }

    protected override void RecalculateStats()
    {
        //physicalResistance = baseStats.physicalResistance;
        //fireResistance = baseStats.fireResistance;
        //lightningResistance = baseStats.lightningResistance;
        //poisonResistance = baseStats.poisonResistance;

        maxHealth = baseStats.maxHealth;
        healthRegen = baseStats.healthRegen;
        moveSpeed = baseStats.moveSpeed;
        angularSpeed = baseStats.angularSpeed;
        //attackSpeed = baseStats.attackSpeed;

        if (effects.Count > 0)
        {
            foreach (Effect effect in effects)
            {
                if (effect is ModifyStatsEffect)
                {
                    ModifyStatsEffect modifyStatsEffect = effect as ModifyStatsEffect;

                    physicalResistance += modifyStatsEffect.physicalResistanceBonus;
                    fireResistance += modifyStatsEffect.fireResistanceBonus;
                    lightningResistance += modifyStatsEffect.lightningResistanceBonus;
                    poisonResistance += modifyStatsEffect.poisonResistanceBonus;

                    int healthBonus = modifyStatsEffect.maxHealthBonus * baseStats.maxHealth;
                    maxHealth += healthBonus;
                    health += healthBonus;

                    //healthRegen += modifyStatsEffect.healthRegenBonus * baseStats.healthRegen;
                    moveSpeed += modifyStatsEffect.speedBonus * baseStats.moveSpeed;
                    angularSpeed += modifyStatsEffect.speedBonus * baseStats.angularSpeed;
                    //attackSpeed += modifyStatsEffect.attackSpeedBonus * baseStats.attackSpeed;
                }
            }
        }
        CheckConstraints();
        UpdateAgent();
    }

    protected override void OnDeath()
    {
        throw new System.NotImplementedException();
    }

    public bool HasItem(Item item)
    {
        return inventory.Contains(item);
    }
}
