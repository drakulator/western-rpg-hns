﻿using UnityEngine;

public class EnemyBaseStats : MonoBehaviour
{
    public int maxHealth = 100;

    [Header("Resistances")]
    [Space]
    public float physicalResistance = 0f;
    public float fireResistance = 0f;
    public float lightningResistance = 0f;
    public float poisonResistance = 0f;

    [Header("Visuals")]
    [Space]
    public GameObject damageEffect;
    public GameObject deathEffect;

    [Header("Regen")]
    [Space]
    public float healthRegen = 1f;

    [Header("Movement")]
    [Space]
    public float moveSpeed = 10f;
    public float angularSpeed = 120f;

    [Header("Combat")]
    [Space]
    public DamageType damageType;
    public int damage = 10;
    public float attackSpeed = 1f;
    public float range = 5f;
    public float critChance = 0.1f;

    [Header("Attack Effects")]
    [Space]
    public Effect[] attackEffects;
}
