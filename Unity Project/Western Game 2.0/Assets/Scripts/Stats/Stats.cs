﻿using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.AI;

public abstract class Stats : MonoBehaviour, IDestroyable
{
    [ShowNonSerializedField] protected int maxHealth = 100;
    [ShowNonSerializedField] protected int health = 100;

    [Header("Resistances")]
    [Space]
    [ShowNonSerializedField] protected float physicalResistance = 0f;
    [ShowNonSerializedField] protected float fireResistance = 0f;
    [ShowNonSerializedField] protected float lightningResistance = 0f;
    [ShowNonSerializedField] protected float poisonResistance = 0f;

    [Header("Visuals")]
    [Space]
    [SerializeField] protected GameObject damageEffect;
    [SerializeField] protected GameObject criticalHitEffect;
    [SerializeField] protected GameObject deathEffect;

    protected List<Effect> effects;

    [Header("Regen")]
    [Space]
    [ShowNonSerializedField] protected float healthRegen = 0.5f;
    protected float healthToRegen = 0f;

    [Header("Movement")]
    [Space]
    [ShowNonSerializedField] protected float moveSpeed = 10f;
    [ShowNonSerializedField] protected float angularSpeed = 120f;

    [Header("Combat")]
    [Space]
    protected DamageType damageType;
    [ShowNonSerializedField] protected int damage = 10;
    [ShowNonSerializedField] protected float attackSpeed = 1f;
    [ShowNonSerializedField] protected float range = 5f;
    [ShowNonSerializedField] protected float critChance = 0.1f;

    [Header("Attack Effects")]
    [Space]
    protected Effect[] attackEffects;

    public void GiveDamage(DamageType type, int damage, bool criticalHit = false, Effect[] additionalEffects = null)
    {
        int damageToGive = damage;
        if (criticalHit)
        {
            damageToGive *= 2;
            Instantiate(criticalHitEffect, transform.position, transform.rotation);
        }
        else
            Instantiate(damageEffect, transform.position, transform.rotation);
        switch (type)
        {
            case DamageType.Physical:
                health -= (int)(damageToGive * (1f - physicalResistance));
                break;
            case DamageType.Fire:
                health -= (int)(damageToGive * (1f - fireResistance));
                break;
            case DamageType.Lightning:
                health -= (int)(damageToGive * (1f - lightningResistance));
                break;
            case DamageType.Poison:
                health -= (int)(damageToGive * (1f - poisonResistance));
                break;
        }
        if (additionalEffects != null)
        {
            foreach (Effect effect in additionalEffects)
            {
                if(effects.TryAddEffect(effect, this))
                    RecalculateStats();
            }
        }
        if (health <= 0)
            OnDeath();
    }

    public void Heal(int health)
    {
        this.health += health;
    }

    public bool TryAddEffect(Effect effect)
    {
        return effects.TryAddEffect(effect, this);
    }

    protected void UpdateEffects()
    {
        for (int i = 0; i < effects.Count; i++)
        {
            Effect effect = effects[i];
            if (effect.HasStopped)
            {
                effects.Remove(effect);
                RecalculateStats();
            }
            else
            {
                Instantiate(effect.VisualEffect, transform.position, transform.rotation);
                effect.Act(this);
            }
        }
    }

    protected void RegenerateHealth()
    {
        if (health < maxHealth)
        {
            healthToRegen += healthRegen;
            if(healthToRegen >= 1f)
            {
                health += (int)healthToRegen;
                healthToRegen -= (int)healthToRegen;
            }
        }
        else
        {
            healthToRegen = 0f;
        }
    }

    protected void CheckConstraints()
    {
        health = Mathf.Clamp(health, 0, maxHealth);
        physicalResistance = Mathf.Clamp(physicalResistance, GameConsts.MAX_VULNERABILITY, GameConsts.MAX_RESISTANCE);
        fireResistance = Mathf.Clamp(fireResistance, GameConsts.MAX_VULNERABILITY, GameConsts.MAX_RESISTANCE);
        lightningResistance = Mathf.Clamp(lightningResistance, GameConsts.MAX_VULNERABILITY, GameConsts.MAX_RESISTANCE);
        poisonResistance = Mathf.Clamp(poisonResistance, GameConsts.MAX_VULNERABILITY, GameConsts.MAX_RESISTANCE);
    }

    protected void UpdateAgent()
    {
        GetComponentInChildren<NavMeshAgent>().speed = moveSpeed;
        GetComponentInChildren<NavMeshAgent>().angularSpeed = angularSpeed;
    }

    protected abstract void RecalculateStats();
    protected abstract void OnDeath();
}
