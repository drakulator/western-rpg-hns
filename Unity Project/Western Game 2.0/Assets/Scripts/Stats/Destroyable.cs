﻿using UnityEngine;
using NaughtyAttributes;

public class Destroyable : MonoBehaviour, IDestroyable
{
    [Header("Base stats")]
    [SerializeField] protected int maxHealth = 100;
    [ReadOnly][SerializeField] protected int health = 100;

    [Header("Resistances")]
    [Space]
    [SerializeField] protected float physicalResistance = 0f;
    [SerializeField] protected float fireResistance = 0f;
    [SerializeField] protected float lightningResistance = 0f;
    [SerializeField] protected float poisonResistance = 0f;

    [Header("Visuals")]
    [Space]
    [SerializeField] protected GameObject damageEffect;
    [SerializeField] protected GameObject criticalHitEffect;
    [SerializeField] protected GameObject deathEffect;

    public void GiveDamage(DamageType type, int damage, bool criticalHit = false, Effect[] additionalEffects = null)
    {
        int damageToGive = damage;
        if (criticalHit)
        {
            damageToGive *= 2;
            Instantiate(criticalHitEffect, transform.position, transform.rotation);
        }
        else
            Instantiate(damageEffect, transform.position, transform.rotation);
        switch (type)
        {
            case DamageType.Physical:
                health -= (int)(damageToGive * (1f - physicalResistance));
                break;
            case DamageType.Fire:
                health -= (int)(damageToGive * (1f - fireResistance));
                break;
            case DamageType.Lightning:
                health -= (int)(damageToGive * (1f - lightningResistance));
                break;
            case DamageType.Poison:
                health -= (int)(damageToGive * (1f - poisonResistance));
                break;
        }
        if (health <= 0)
            OnDeath();
    }

    private void OnDeath()
    {
        Instantiate(deathEffect, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
