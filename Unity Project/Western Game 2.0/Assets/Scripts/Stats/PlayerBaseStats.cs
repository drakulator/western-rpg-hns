﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerBaseStats : MonoBehaviour
{
    private static int[] experienceTable =
    {
        0,
        100,
        500,
        1000,
        2000,
        4000,
        6000,
        8000,
        10000,
        12000,
        15000,
        20000,
        50000,
        75000,
        100000,
        150000,
        200000,
        400000,
        600000,
        1000000
    };

    public int experiencePoints = 0;
    public int attributePoints;
    public int skillPoints;
    public int level = 1;

    [Space]
    public int maxHealth = 100;

    [Header("Visuals")]
    [Space]
    public GameObject damageEffect;
    public GameObject deathEffect;

    [Header("Regen")]
    [Space]
    public float healthRegen = 1f;

    [Header("Movement")]
    [Space]
    public float moveSpeed = 10f;
    public float angularSpeed = 120f;

    [Header("Attributes")]
    [SerializeField] private int strength = 5;
    [SerializeField] private int dexterity = 5;
    [SerializeField] private int intelligence = 5;
    [SerializeField] private int endurance = 5;

    [Header("Skills")]
    private Dictionary<Skill, int> skills = new Dictionary<Skill, int>
    {
        { Skill.Lockpicking, 0 },
        { Skill.Persuasion, 0 },
        { Skill.Herbalism, 0 },
        { Skill.Engineering, 0 },
        { Skill.Explosives, 0 },
        { Skill.Traps, 0 },
        { Skill.Lore, 0 }
    };

    [Header("Weapon Proficiencies")]
    private Dictionary<WeaponType, int> proficiencies = new Dictionary<WeaponType, int>
    {
        { WeaponType.Spears, 0 },
        { WeaponType.Knives, 0 },
        { WeaponType.Axes, 0 },
        { WeaponType.Bows, 0 },
        { WeaponType.Handguns, 0 },
        { WeaponType.Rifles, 0 }
    };

    private void OnXPGained(object sender, int experiencePoints)
    {
        this.experiencePoints += experiencePoints;
        if (this.experiencePoints >= experienceTable[level])
            LevelUp();
    }

    private void LevelUp()
    {
        maxHealth += 10;
        attributePoints += 1;
        skillPoints += 1;
        level++;

        EventManager.Invoke("PlayerBaseStats_Changed", this);
    }

    private void IncreaseStrength(object sender)
    {
        attributePoints--;
        strength++;
        EventManager.Invoke("PlayerBaseStats_Changed", this);
    }

    private void IncreaseDexterity(object sender)
    {
        attributePoints--;
        dexterity++;
        EventManager.Invoke("PlayerBaseStats_Changed", this);
    }

    private void IncreaseIntelligence(object sender)
    {
        attributePoints--;
        intelligence++;
        skillPoints += 2;
        EventManager.Invoke("PlayerBaseStats_Changed", this);
    }

    private void IncreaseEndurance(object sender)
    {
        attributePoints--;
        endurance++;
        EventManager.Invoke("PlayerBaseStats_Changed", this);
    }

    private void IncreaseSkill(object sender, Skill skill)
    {
        skillPoints--;
        skills[skill]++;
        EventManager.Invoke("PlayerBaseStats_SkillsChanged", this);
    }

    private void IncreaseProficiency(object sender, WeaponType weaponType)
    {
        skillPoints--;
        proficiencies[weaponType]++;
        EventManager.Invoke("PlayerBaseStats_ProficienciesChanged", this);
    }
}
