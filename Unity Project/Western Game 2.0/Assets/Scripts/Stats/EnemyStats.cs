﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyBaseStats))]
public class EnemyStats : Stats
{
    private EnemyBaseStats baseStats;

    // Use this for initialization
    void Start()
    {
        baseStats = GetComponent<EnemyBaseStats>();

        InitiateStats();
        CheckConstraints();
        UpdateAgent();

        InvokeRepeating("RegenerateHealth", 1f, 1f);
    }

    private void InitiateStats()
    {
        maxHealth = baseStats.maxHealth;
        health = baseStats.maxHealth;
        physicalResistance = baseStats.physicalResistance;
        fireResistance = baseStats.fireResistance;
        lightningResistance = baseStats.lightningResistance;
        poisonResistance = baseStats.poisonResistance;
        damageEffect = baseStats.damageEffect;
        deathEffect = baseStats.deathEffect;
        healthRegen = baseStats.healthRegen;
        moveSpeed = baseStats.moveSpeed;
        angularSpeed = baseStats.angularSpeed;
        damageType = baseStats.damageType;
        damage = baseStats.damage;
        attackSpeed = baseStats.attackSpeed;
        range = baseStats.range;
        critChance = baseStats.critChance;
        attackEffects = baseStats.attackEffects;

        effects = new List<Effect>();
    }

    protected override void RecalculateStats()
    {
        physicalResistance = baseStats.physicalResistance;
        fireResistance = baseStats.fireResistance;
        lightningResistance = baseStats.lightningResistance;
        poisonResistance = baseStats.poisonResistance;

        maxHealth = baseStats.maxHealth;
        healthRegen = baseStats.healthRegen;
        moveSpeed = baseStats.moveSpeed;
        angularSpeed = baseStats.angularSpeed;
        attackSpeed = baseStats.attackSpeed;

        if(effects.Count > 0)
        {
            foreach (Effect effect in effects)
            {
                if(effect is ModifyStatsEffect)
                {
                    ModifyStatsEffect modifyStatsEffect = effect as ModifyStatsEffect;

                    physicalResistance += modifyStatsEffect.physicalResistanceBonus;
                    fireResistance += modifyStatsEffect.fireResistanceBonus;
                    lightningResistance += modifyStatsEffect.lightningResistanceBonus;
                    poisonResistance += modifyStatsEffect.poisonResistanceBonus;

                    int healthBonus = modifyStatsEffect.maxHealthBonus * baseStats.maxHealth;
                    maxHealth += healthBonus;
                    health += healthBonus;

                    healthRegen += modifyStatsEffect.healthRegenBonus * baseStats.healthRegen;
                    moveSpeed += modifyStatsEffect.speedBonus * baseStats.moveSpeed;
                    angularSpeed += modifyStatsEffect.speedBonus * baseStats.angularSpeed;
                    attackSpeed += modifyStatsEffect.attackSpeedBonus * baseStats.attackSpeed;
                }
            }
        }
        CheckConstraints();
        UpdateAgent();
    }

    protected override void OnDeath()
    {
        throw new System.NotImplementedException();
    }
}
