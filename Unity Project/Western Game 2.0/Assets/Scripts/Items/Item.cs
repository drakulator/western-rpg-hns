﻿using UnityEngine;
using NaughtyAttributes;

public abstract class Item : ScriptableObject
{
    [Tooltip("Stackable objects having the same id can be stacked")]
    public int id;
    public string itemName;
    public int cost;
    public ItemType itemType;
    [ShowNativeProperty] public bool IsStackable => itemType == ItemType.Consumable || itemType == ItemType.Miscellaneous;
    [TextArea(3, 10)]
    public string description;
    public Sprite sprite;
    public bool isEssential;

    public static bool CanBeStacked(Item item1, Item item2)
    {
        return item1.IsStackable && item2.IsStackable && item1.id == item2.id;
    }
}
