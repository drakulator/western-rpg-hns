﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    private bool rangedWeaponEquipped;

    private bool inventoryPanelOpened;
    private bool characterSheetOpened;

    private bool IsPanelOpened => inventoryPanelOpened || characterSheetOpened;

    private void OnEnable()
    {
        EventManager.AddListener("InventoryClosed", OnInventoryClosed);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener("InventoryClosed", OnInventoryClosed);
    }

    private void OnInventoryClosed(object sender)
    {
        inventoryPanelOpened = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Inventory"))
        {
            if (inventoryPanelOpened)
            {
                EventManager.Invoke("InventoryClosed", this);
            }
            else
            {
                EventManager.Invoke("InventoryOpened", this, InventoryType.Player);
                EventManager.Invoke("StopPlayerMovement", this);
                inventoryPanelOpened = true;
            }
        }

        if (Input.GetMouseButton(0) && !IsPanelOpened)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                if (rangedWeaponEquipped && Input.GetButton("Fire in Place"))
                {
                    EventManager.Invoke("Input_ShootAt", this, hit.point);
                }
                else
                {
                    switch (hit.transform.root.tag)
                    {
                        case "Destroyable":
                        case "Enemy":
                        case "Boss":
                            EventManager.Invoke("Input_Attack", this, hit.collider);
                            break;
                        case "Neutral":
                        case "Ally":
                        case "Container":
                        case "Collectible":
                        case "Interactable":
                            EventManager.Invoke("Input_Interact", this, hit.collider);
                            break;
                        default:
                            EventManager.Invoke("Input_MoveTo", this, hit.point);
                            break;
                    }
                }
            }
        }
    }
}
