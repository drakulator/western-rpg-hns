﻿using TMPro;
using UnityEngine;

public class UIInventoryMoney : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI moneyText;

    private void OnEnable()
    {
        EventManager.AddListener<int>("MoneyChanged", OnMoneyChanged);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<int>("MoneyChanged", OnMoneyChanged);
    }

    private void OnMoneyChanged(object sender, int money)
    {
        moneyText.text = "" + money;
    }
}
