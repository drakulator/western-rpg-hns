﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIInventorySlot : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public UISlotEvent itemTaken;
    public UISlotEvent itemDropped;
    public UISlotEvent itemSelected;

    private static bool ItemDragged = false;

    [SerializeField]
    private UIInventoryItem inventoryItem;

    private int index = -1;
    public int Index
    {
        get
        {
            return index;
        }

        set
        {
            if (index == -1)
                index = value;
            else
                Debug.LogError("Slot indexes should only be assigned once!");
        }
    }

    private void Awake()
    {
        itemTaken = new UISlotEvent();
        itemDropped = new UISlotEvent();
        itemSelected = new UISlotEvent();
    }

    private void OnEnable()
    {
        inventoryItem.itemTaken.AddListener(OnItemTaken);
    }

    private void OnDisable()
    {
        inventoryItem.itemTaken.RemoveListener(OnItemTaken);
    }

    private void OnItemTaken()
    {
        itemTaken.Invoke(this);
        ItemDragged = true;
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (ItemDragged)
        {
            itemDropped.Invoke(this);
            ItemDragged = false;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.pointerId == -2 && inventoryItem.HasItem)
        {
            itemSelected.Invoke(this);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {

    }

    public void OnPointerExit(PointerEventData eventData)
    {

    }

    public void LoadItem(ItemQuantity itemQuantity)
    {
        inventoryItem.UpdateDisplay(itemQuantity);
    }
}

[System.Serializable]
public class UISlotEvent : UnityEvent<UIInventorySlot> { }