﻿using UnityEngine;

public class UIAdditionalInventoryPanels : MonoBehaviour
{
    [SerializeField]
    private UIInventoryActionPanel actionPanel;
    [SerializeField]
    private UIInventorySplitPanel splitPanel;

    private void OnEnable()
    {
        EventManager.AddListener<ItemQuantity, bool>("ActionPanelOpened", OnActionPanelOpened);
        EventManager.AddListener<int>("SplitPanelOpened", OnSplitPanelOpened);
        EventManager.AddListener("SplitPanelClosed", OnSplitPanelClosed);
        EventManager.AddListener("InventoryClosed", OnInventoryClosed);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<ItemQuantity, bool>("ActionPanelOpened", OnActionPanelOpened);
        EventManager.RemoveListener<int>("SplitPanelOpened", OnSplitPanelOpened);
        EventManager.RemoveListener("SplitPanelClosed", OnSplitPanelClosed);
        EventManager.RemoveListener("InventoryClosed", OnInventoryClosed);
    }

    public void OnActionPanelOpened(object sender, ItemQuantity itemQuantity, bool hasEmptySlot)
    {
        actionPanel.gameObject.SetActive(true);
        actionPanel.transform.position = Input.mousePosition;
        actionPanel.SetActionsEnabled(itemQuantity, hasEmptySlot);
        EventManager.AddListener<InventoryActionType>("ActionSelected", OnActionSelected);
    }

    private void OnActionSelected(object sender, InventoryActionType action)
    {
        EventManager.RemoveListener<InventoryActionType>("ActionSelected", OnActionSelected);
        actionPanel.gameObject.SetActive(false);
    }

    private void OnSplitPanelOpened(object sender, int quantity)
    {
        splitPanel.gameObject.SetActive(true);
        splitPanel.transform.position = Input.mousePosition;
        splitPanel.InitializeValues(quantity);
    }

    private void OnSplitPanelClosed(object sender)
    {
        splitPanel.gameObject.SetActive(false);
    }

    private void OnInventoryClosed(object sender)
    {
        actionPanel.gameObject.SetActive(false);
        splitPanel.gameObject.SetActive(false);
    }
}
