﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIInventoryItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public UnityEvent itemTaken;

    public bool HasItem { get; private set; }

    [SerializeField]
    private TextMeshProUGUI quantityText;

    private Transform originalParent;

    private void Awake()
    {
        itemTaken = new UnityEvent();
    }

    private void Start()
    {
        originalParent = transform.parent;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (eventData.pointerId == -1 && HasItem)
        {
            transform.SetParent(transform.root);
            transform.position = eventData.position;
            GetComponent<CanvasGroup>().blocksRaycasts = false;

            itemTaken.Invoke();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (HasItem)
        {
            transform.position = eventData.position;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(originalParent);
        transform.localPosition = Vector2.zero;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void UpdateDisplay(ItemQuantity itemQuantity)
    {
        if (!itemQuantity.IsEmpty)
        {
            HasItem = true;
            if (itemQuantity.quantity > 1)
                quantityText.text = "" + itemQuantity.quantity;
            else
                quantityText.text = "";
            GetComponent<Image>().enabled = true;
            GetComponent<Image>().sprite = itemQuantity.item.sprite;
        }
        else
        {
            HasItem = false;
            quantityText.text = "";
            GetComponent<Image>().enabled = false;
        }
    }
}
