﻿using System;
using UnityEngine;

public class UISlotsManager : MonoBehaviour
{
    private static Inventory originalInventory;
    private static int originalIndex;

    private Inventory linkedInventory;

    private UIInventorySlot[] inventorySlots;
    private UIInventorySlot selectedSlot;

    public bool HasEmptySlot
    {
        get
        {
            foreach (ItemQuantity itemQuantity in linkedInventory.Items)
            {
                if (itemQuantity.IsEmpty)
                    return true;
            }
            return false;
        }
    }

    private void Awake()
    {
        inventorySlots = GetComponentsInChildren<UIInventorySlot>(true);
    }

    private void OnEnable()
    {
        EventManager.AddListener("InventoryChanged", OnInventoryChanged);
        foreach (UIInventorySlot inventorySlot in inventorySlots)
        {
            inventorySlot.itemTaken.AddListener(OnItemTaken);
            inventorySlot.itemDropped.AddListener(OnItemDropped);
            inventorySlot.itemSelected.AddListener(OnItemSelected);
        }
    }

    private void OnDisable()
    {
        EventManager.RemoveListener("InventoryChanged", OnInventoryChanged);
        foreach (UIInventorySlot inventorySlot in inventorySlots)
        {
            inventorySlot.itemTaken.RemoveListener(OnItemTaken);
            inventorySlot.itemDropped.RemoveListener(OnItemDropped);
            inventorySlot.itemSelected.RemoveListener(OnItemSelected);
        }
    }

    private void Start()
    {
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            inventorySlots[i].Index = i;
        }
    }

    private void OnItemTaken(UIInventorySlot slot)
    {
        originalInventory = linkedInventory;
        originalIndex = slot.Index;
    }

    private void OnItemDropped(UIInventorySlot slot)
    {
        Inventory.ExchangeItems(originalInventory, originalIndex, linkedInventory, slot.Index);
        EventManager.Invoke("InventoryChanged", this);
    }

    public void OnInventoryPanelChanged(Inventory inventory)
    {
        linkedInventory = inventory;
        for (int i = 0; i < inventory.Items.Length; i++)
        {
            inventorySlots[i].LoadItem(inventory.Items[i]);
        }
    }

    private void OnInventoryChanged(object sender)
    {
        for (int i = 0; i < linkedInventory.Items.Length; i++)
        {
            inventorySlots[i].LoadItem(linkedInventory.Items[i]);
        }
    }

    private void OnItemSelected(UIInventorySlot slot)
    {
        selectedSlot = slot;
        EventManager.Invoke("ActionPanelOpened", this, linkedInventory.GetItem(slot.Index), HasEmptySlot);
        EventManager.AddListener<InventoryActionType>("ActionSelected", OnActionSelected);
    }

    private void OnActionSelected(object sender, InventoryActionType action)
    {
        EventManager.RemoveListener<InventoryActionType>("ActionSelected", OnActionSelected);
        linkedInventory.ExecuteAction(selectedSlot.Index, action);
    }
}