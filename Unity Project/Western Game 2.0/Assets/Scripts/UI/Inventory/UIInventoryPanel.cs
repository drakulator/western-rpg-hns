﻿using UnityEngine;

public class UIInventoryPanel : MonoBehaviour
{
    [SerializeField]
    private InventoryType inventoryType;
    public InventoryType InventoryType { get { return inventoryType; } }

    [SerializeField]
    private GameObject inventoryPanel;

    [SerializeField]
    private UISlotsManager slotsManager;

    private void OnEnable()
    {
        EventManager.AddListener("InventoryLoaded", OnInventoryLoaded);
        EventManager.AddListener("InventoryClosed", OnInventoryClosed);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener("InventoryLoaded", OnInventoryLoaded);
        EventManager.RemoveListener("InventoryClosed", OnInventoryClosed);
    }

    private void OnInventoryClosed(object sender)
    {
        inventoryPanel.SetActive(false);
    }

    public void OnInventoryLoaded(object sender)
    {
        Inventory loadedInventory = sender as Inventory;
        if (loadedInventory != null && loadedInventory.InventoryType == InventoryType)
        {
            inventoryPanel.SetActive(true);
            slotsManager.OnInventoryPanelChanged(loadedInventory);
            EventManager.Invoke("MoneyChanged", this, loadedInventory.Money);
        }
    }
}
