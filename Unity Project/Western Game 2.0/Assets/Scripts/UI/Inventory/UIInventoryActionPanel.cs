﻿using UnityEngine;
using UnityEngine.UI;

public class UIInventoryActionPanel : MonoBehaviour
{
    [SerializeField]
    private Button UseButton;
    [SerializeField]
    private Button SplitButton;
    [SerializeField]
    private Button RemoveButton;

    public void SetActionsEnabled(ItemQuantity itemQuantity, bool hasEmptySlot)
    {
        UseButton.interactable = itemQuantity.item.itemType == ItemType.Consumable;
        SplitButton.interactable = itemQuantity.quantity > 1 && itemQuantity.item.IsStackable;
        RemoveButton.interactable = !itemQuantity.item.isEssential;
    }

    public void B_Use()
    {
        EventManager.Invoke("ActionSelected", this, InventoryActionType.Use);
    }

    public void B_Split()
    {
        EventManager.Invoke("ActionSelected", this, InventoryActionType.Split);
    }

    public void B_Remove()
    {
        EventManager.Invoke("ActionSelected", this, InventoryActionType.Remove);
    }

    public void B_Cancel()
    {
        EventManager.Invoke("ActionSelected", this, InventoryActionType.Cancel);
    }
}
