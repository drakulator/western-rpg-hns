﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIInventorySplitPanel : MonoBehaviour {

    [SerializeField]
    private Slider slider;
    [SerializeField]
    private TextMeshProUGUI valueText1;
    [SerializeField]
    private TextMeshProUGUI valueText2;

    private int value1;
    private int value2;

    public void InitializeValues(int quantity)
    {
        value1 = quantity - 1;
        value2 = 1;

        valueText1.text = "" + value1;
        valueText2.text = "" + value2;

        slider.value = 1;
        slider.maxValue = value1;

        slider.onValueChanged.AddListener(delegate { UpdateValues(); } );
    }

    private void OnDisable()
    {
        slider.onValueChanged.RemoveListener(delegate { UpdateValues(); });
    }

    private void UpdateValues()
    {
        value1 = (int)(slider.maxValue - slider.value + 1);
        value2 = (int)slider.value;

        valueText1.text = "" + value1;
        valueText2.text = "" + value2;
    }

    public void B_OK()
    {
        EventManager.Invoke("SplitPanelValuesSelected", this, value2);
        EventManager.Invoke("SplitPanelClosed", this);
    }

    public void B_Cancel()
    {
        EventManager.Invoke("SplitPanelClosed", this);
    }
}
