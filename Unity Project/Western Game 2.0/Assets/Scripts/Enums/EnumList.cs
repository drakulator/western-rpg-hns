﻿public enum DamageType
{
    Physical,
    Fire,
    Lightning,
    Poison
}

public enum Skill
{
    Lockpicking,
    Persuasion,
    Herbalism,
    Engineering,
    Explosives,
    Traps,
    Lore
}

public enum WeaponType
{
    Spears,
    Knives,
    Axes,
    Bows,
    Handguns,
    Rifles
}

public enum ItemType
{
    Weapon,
    Armor,
    Consumable,
    Miscellaneous
}

public enum InventoryType
{
    Player,
    Container
}

public enum InventoryActionType
{
    Use,
    Split,
    Remove,
    Cancel
}

public enum PlayerActionType
{
    None,
    Interact,
    Attack
}