﻿using System.Collections.Generic;
using UnityEngine;

public static partial class MyExtensions
{
    public static bool TryAddEffect(this List<Effect> list, Effect effect, Stats stats)
    {
        float roll = Random.value;
        if(roll <= effect.ChanceToActivate)
        {
            Effect existingEffect;
            if (existingEffect = list.Find(x => x.EffectName.Equals(effect.EffectName)))
            {
                existingEffect.Reset(stats);
            }
            else
            {
                list.Add(effect.Create());
                effect.StartEffect(stats);
            }
            return true;
        }
        return false;
    }
}
