﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    private bool isFollowing;

    [SerializeField]
    private float xOffset = -9;
    [SerializeField]
    private float yOffset = 8;
    [SerializeField]
    private float zOffset = -10;

    private PlayerController player;

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        isFollowing = true;
    }

    void LateUpdate()
    {
        if (isFollowing)
            transform.position = new Vector3(player.transform.position.x + xOffset, player.transform.position.y + yOffset, player.transform.position.z + zOffset);
    }
}

