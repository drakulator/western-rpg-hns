﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerController : MonoBehaviour
{
    private NavMeshAgent agent;
    private PlayerStats stats;

    private bool waitingBetweenAttacks = false;

    public static PlayerActionType CurrentAction { get; private set; } = PlayerActionType.None;
    private IInteractable targetInteractableComponent;
    private IDestroyable targetStats;
    private Vector3 targetPosition;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        stats = GetComponent<PlayerStats>();
    }

    private void OnEnable()
    {
        EventManager.AddListener<Vector3>("Input_ShootAt", ShootAt);
        EventManager.AddListener<Vector3>("Input_MoveTo", MoveTo);
        EventManager.AddListener<Collider>("Input_Attack", Attack);
        EventManager.AddListener<Collider>("Input_Interact", Interact);

        EventManager.AddListener("StopPlayerMovement", StopMovement);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<Vector3>("Input_ShootAt", ShootAt);
        EventManager.RemoveListener<Vector3>("Input_MoveTo", MoveTo);
        EventManager.RemoveListener<Collider>("Input_Attack", Attack);
        EventManager.RemoveListener<Collider>("Input_Interact", Interact);

        EventManager.RemoveListener("StopPlayerMovement", StopMovement);
    }

    private void ShootAt(object sender, Vector3 target)
    {

    }

    private void MoveTo(object sender, Vector3 target)
    {
        CurrentAction = PlayerActionType.None;
        agent.destination = target;
    }

    private void Attack(object sender, Collider target)
    {
        targetPosition = target.transform.position;
        targetStats = target.GetComponent<IDestroyable>();
        if (targetStats != null)
        {
            CurrentAction = PlayerActionType.Attack;
        }
    }

    private void Interact(object sender, Collider target)
    {
        targetPosition = target.transform.position;
        targetInteractableComponent = target.GetComponent<IInteractable>();
        if (targetInteractableComponent != null)
        {
            CurrentAction = PlayerActionType.Interact;
        }
    }

    private void StopMovement(object sender = null)
    {
        agent.destination = transform.position;
    }

    private void Update()
    {
        ExecuteCurrentAction();
    }

    private void ExecuteCurrentAction()
    {
        if (CurrentAction == PlayerActionType.None)
            return;
        if (CurrentAction == PlayerActionType.Attack)
        {
            float distance = Vector3.Distance(transform.position, targetPosition);
            if (distance <= stats.Range)
            {
                StopMovement();

                // Rotate towards target
                Vector3 direction = (transform.position - targetPosition).normalized;
                Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * agent.angularSpeed);

                if (stats.WeaponEquipped && !waitingBetweenAttacks)
                    StartCoroutine(Attack(targetStats, stats.AttackSpeed, stats.DamageType, stats.Damage, stats.CritChance, stats.AttackEffects));
                CurrentAction = PlayerActionType.None;
            }
            else
            {
                agent.destination = targetPosition;
            }
        }
        if (CurrentAction == PlayerActionType.Interact)
        {
            float distance = Vector3.Distance(transform.position, targetPosition);
            if (distance <= GameConsts.INTERACTION_RANGE)
            {
                StopMovement();

                // Rotate towards target
                Vector3 direction = (transform.position - targetPosition).normalized;
                Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * agent.angularSpeed);

                targetInteractableComponent.Interact();
                CurrentAction = PlayerActionType.None;
            }
            else
            {
                agent.destination = targetPosition;
            }
        }
    }

    private IEnumerator Attack(IDestroyable target, float attackSpeed, DamageType damageType, int damage, float critChance, Effect[] attackEffects)
    {
        bool criticalHit = critChance <= Random.value;

        waitingBetweenAttacks = true;
        yield return new WaitForSeconds(0.5f / attackSpeed);
        target.GiveDamage(damageType, damage, criticalHit, attackEffects);
        yield return new WaitForSeconds(0.5f / attackSpeed);
        waitingBetweenAttacks = false;
    }

}
