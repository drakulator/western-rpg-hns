﻿using UnityEngine;

public class ModifyStatsEffect : Effect
{
    public int maxHealthBonus;

    public float fireResistanceBonus;
    public float lightningResistanceBonus;
    public float poisonResistanceBonus;
    public float physicalResistanceBonus;

    public int healthRegenBonus;

    public float speedBonus;
    public float attackSpeedBonus;

    public override void StartEffect(Stats controller)
    {
        timeLeft = time;
        Debug.Log($"{effectName} activated");
    }

    public override void Act(Stats controller)
    {
        timeLeft--;
        Debug.Log($"{effectName} lasts");
    }

    public override void StopEffect(Stats controller)
    {
        Debug.Log($"{effectName} stopped");
    }

    public override void Reset(Stats controller)
    {
        timeLeft = time;
        Debug.Log($"{effectName} renewed");
    }

    public override Effect Create()
    {
        ModifyStatsEffect effect = CreateInstance<ModifyStatsEffect>();

        effect.effectName = effectName;
        effect.time = time;
        effect.chanceToActivate = chanceToActivate;
        effect.visualEffect = visualEffect;
        effect.maxHealthBonus = maxHealthBonus;
        effect.fireResistanceBonus = fireResistanceBonus;
        effect.lightningResistanceBonus = lightningResistanceBonus;
        effect.poisonResistanceBonus = poisonResistanceBonus;
        effect.physicalResistanceBonus = physicalResistanceBonus;
        effect.healthRegenBonus = healthRegenBonus;
        effect.speedBonus = speedBonus;
        effect.attackSpeedBonus = attackSpeedBonus;

        return effect;
    }
}
