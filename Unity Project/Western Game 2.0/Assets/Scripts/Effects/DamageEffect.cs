﻿using UnityEngine;

[CreateAssetMenu(menuName = "Effects/DamageEffect")]
public class DamageEffect : Effect
{
    public DamageType damageType;
    public int damage;

    public override void StartEffect(Stats controller)
    {
        timeLeft = time - 1;
        controller.GiveDamage(damageType, damage);
        Debug.Log($"{effectName} activated");
    }

    public override void Act(Stats controller)
    {
        controller.GiveDamage(damageType, damage);
        timeLeft--;
        Debug.Log($"{effectName} lasts");
    }

    public override void StopEffect(Stats controller)
    {
        Debug.Log($"{effectName} stopped");
    }

    public override string ToString()
    {
        return $"{chanceToActivate * 100}% for {damage} points of {damageType} damage for {time} seconds\n";
    }

    public override Effect Create()
    {
        DamageEffect effect = CreateInstance<DamageEffect>();

        effect.effectName = effectName;
        effect.time = time;
        effect.chanceToActivate = chanceToActivate;
        effect.visualEffect = visualEffect;
        effect.damageType = damageType;
        effect.damage = damage;

        return effect;
    }
}
