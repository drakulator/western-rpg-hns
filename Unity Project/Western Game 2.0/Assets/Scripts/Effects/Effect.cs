﻿using UnityEngine;

public abstract class Effect : ScriptableObject
{
    [SerializeField] protected string effectName;
    [SerializeField] protected int time;
    protected int timeLeft;
    [SerializeField] protected float chanceToActivate;
    [SerializeField] protected GameObject visualEffect;

    public string EffectName { get { return effectName; } }
    public float ChanceToActivate { get { return chanceToActivate; } }
    public bool HasStopped { get { return timeLeft <= 0; } }
    public GameObject VisualEffect { get { return visualEffect; } }

    public abstract void StartEffect(Stats controller);
    public abstract void Act(Stats controller);
    public abstract void StopEffect(Stats controller);

    public virtual void Reset(Stats controller)
    {
        StartEffect(controller);
    }

    public abstract Effect Create();
}