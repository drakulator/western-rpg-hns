﻿using UnityEngine;

[CreateAssetMenu(menuName = "Effects/HealEffect")]
public class HealEffect : Effect
{
    public int healing;

    public override void StartEffect(Stats controller)
    {
        timeLeft = time - 1;
        controller.Heal(healing);
        Debug.Log($"{effectName} activated");
    }

    public override void Act(Stats controller)
    {
        controller.Heal(healing);
        time--;
        Debug.Log($"{effectName} lasts");
    }

    public override void StopEffect(Stats controller)
    {
        Debug.Log($"{effectName} stopped");
    }

    public override string ToString()
    {
        return $"{chanceToActivate * 100}% for {healing} points of healing for {time} seconds\n";
    }

    public override Effect Create()
    {
        HealEffect effect = CreateInstance<HealEffect>();

        effect.effectName = effectName;
        effect.time = time;
        effect.chanceToActivate = chanceToActivate;
        effect.visualEffect = visualEffect;
        effect.healing = healing;

        return effect;
    }
}
